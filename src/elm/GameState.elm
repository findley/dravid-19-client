module GameState exposing
    ( GameMode(..)
    , GameSettings
    , GameState
    , Player
    , PlayerId
    , defaultSettings
    , gameStateDecoder
    , newGame
    , getPlayerName
    )

import Dict exposing (Dict)
import Json.Decode as D


type alias GameState =
    { gameMode : GameMode
    , settings : GameSettings
    , round : Int
    , turn : Int
    , drawer : PlayerId
    , host : PlayerId
    , word : String
    , timeRemainingMs : Int
    , winners : Dict PlayerId Int
    , scores : Dict PlayerId Int
    , players : Dict PlayerId Player
    , turnOrder : List PlayerId
    }


type GameMode
    = PreGame
    | WordSelect (List String)
    | Drawing
    | TurnOver
    | GameOver


type alias PlayerId =
    String


type alias Player =
    { name : String
    , id : PlayerId
    , connected : Bool
    , color : Int
    }


type alias GameSettings =
    { rounds : Int
    , drawSeconds : Int
    }


newGame : GameState
newGame =
    { gameMode = PreGame
    , settings = defaultSettings
    , round = 1
    , turn = 1
    , drawer = ""
    , host = ""
    , word = ""
    , timeRemainingMs = 0
    , winners = Dict.empty
    , scores = Dict.empty
    , players = Dict.empty
    , turnOrder = []
    }


defaultSettings : GameSettings
defaultSettings =
    { rounds = 3
    , drawSeconds = 80
    }


getPlayerName : GameState -> String -> String
getPlayerName game playerId =
    Dict.get playerId game.players
        |> Maybe.map (\p -> p.name)
        |> Maybe.withDefault "<unknown>"


decodeApply : D.Decoder a -> D.Decoder (a -> b) -> D.Decoder b
decodeApply value partial =
    D.andThen (\p -> D.map p value) partial


gameStateDecoder : D.Decoder GameState
gameStateDecoder =
    D.succeed GameState
        |> decodeApply (D.field "game_mode" gameModeDecoder)
        |> decodeApply (D.field "settings" settingsDecoder)
        |> decodeApply (D.field "round" D.int)
        |> decodeApply (D.field "turn" D.int)
        |> decodeApply (D.field "drawer" D.string)
        |> decodeApply (D.field "host" D.string)
        |> decodeApply (D.field "word" D.string)
        |> decodeApply (D.field "time_remaining_ms" D.int)
        |> decodeApply (D.field "winners" (D.dict D.int))
        |> decodeApply (D.field "scores" (D.dict D.int))
        |> decodeApply (D.field "players" (D.dict playerDecoder))
        |> decodeApply (D.field "turn_order" (D.list D.string))


gameModeDecoder : D.Decoder GameMode
gameModeDecoder =
    let
        msgFromType : String -> D.Decoder GameMode
        msgFromType string =
            case string of
                "pre_game" ->
                    D.succeed PreGame

                "word_select" ->
                    D.map WordSelect (D.field "content" (D.list D.string))

                "drawing" ->
                    D.succeed Drawing

                "turn_over" ->
                    D.succeed TurnOver

                "game_over" ->
                    D.succeed GameOver

                _ ->
                    D.fail ("Invalid msg type: " ++ string)
    in
    D.field "type" D.string
        |> D.andThen msgFromType


playerDecoder : D.Decoder Player
playerDecoder =
    D.map4
        Player
        (D.field "name" D.string)
        (D.field "id" D.string)
        (D.field "connected" D.bool)
        (D.field "color" D.int)


settingsDecoder : D.Decoder GameSettings
settingsDecoder =
    D.map2 GameSettings (D.field "rounds" D.int) (D.field "draw_time" D.int)
