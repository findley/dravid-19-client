module Identity exposing (Identity, decode, encode, none)

import Json.Decode as D
import Json.Encode as E
import SHA256


type alias Identity =
    { clientId : String
    , hashId : String
    , name : String
    }


none : Identity
none =
    Identity "" "" ""


encode : Identity -> E.Value
encode identity =
    E.object
        [ ( "clientId", E.string identity.clientId )
        , ( "name", E.string identity.name )
        ]


decode : D.Value -> Maybe Identity
decode value =
    let
        hashId clientId =
            clientId |> SHA256.fromString |> SHA256.toHex |> String.left 10
    in
    D.decodeValue decoder value
        |> Result.toMaybe
        |> Maybe.map (\identity -> { identity | hashId = hashId identity.clientId })


decoder : D.Decoder Identity
decoder =
    D.map3 Identity (D.field "clientId" D.string) (D.succeed "") (D.field "name" D.string)
