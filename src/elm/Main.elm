module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Html exposing (div, text)
import Identity exposing (Identity)
import Json.Decode
import Page.Home
import Page.Lobby
import Route exposing (Route, parseRoute)
import Url


main : Program Json.Decode.Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.page of
        HomePage homeModel ->
            Page.Home.subscriptions homeModel |> Sub.map GotHomeMsg

        LobbyPage lobbyModel ->
            Page.Lobby.subscriptions lobbyModel |> Sub.map GotLobbyMsg

        _ ->
            Sub.none


type alias Model =
    { key : Nav.Key
    , route : Route
    , page : Page
    , url : Url.Url
    }


type Page
    = HomePage Page.Home.Model
    | LobbyPage Page.Lobby.Model
    | NotFoundPage Identity


toIdentity : Page -> Identity
toIdentity page =
    case page of
        HomePage homeModel ->
            homeModel.identity

        LobbyPage lobbyModel ->
            lobbyModel.identity

        NotFoundPage identity ->
            identity


type Msg
    = UrlChanged Url.Url
    | LinkClicked Browser.UrlRequest
    | GotHomeMsg Page.Home.Msg
    | GotLobbyMsg Page.Lobby.Msg


changeRouteTo : Route -> Model -> ( Model, Cmd Msg )
changeRouteTo route model =
    let
        identity =
            toIdentity model.page
    in
    case route of
        Route.NotFound ->
            ( { model | page = NotFoundPage identity }, Cmd.none )

        Route.Home maybeLobbyId ->
            Page.Home.init identity model.key maybeLobbyId
                |> mapUpdate HomePage GotHomeMsg model

        Route.Lobby lobbyId ->
            Page.Lobby.init identity model.key model.url lobbyId
                |> mapUpdate LobbyPage GotLobbyMsg model


init : Json.Decode.Value -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init identity url key =
    let
        route =
            parseRoute url
    in
    { key = key
    , route = route
    , page = NotFoundPage (Identity.decode identity |> Maybe.withDefault Identity.none)
    , url = url
    }
        |> changeRouteTo route


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( UrlChanged url, _ ) ->
            changeRouteTo (parseRoute url) { model | url = url }

        ( LinkClicked request, _ ) ->
            case request of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        ( GotHomeMsg homeMsg, HomePage homeModel ) ->
            Page.Home.update homeMsg homeModel
                |> mapUpdate HomePage GotHomeMsg model

        ( GotLobbyMsg lobbyMsg, LobbyPage lobbyModel ) ->
            Page.Lobby.update lobbyMsg lobbyModel
                |> mapUpdate LobbyPage GotLobbyMsg model

        _ ->
            ( model, Cmd.none )


mapUpdate : (subModel -> Page) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
mapUpdate toPage toMsg model ( subModel, subCmd ) =
    ( { model | page = toPage subModel }, Cmd.map toMsg subCmd )


view : Model -> Browser.Document Msg
view model =
    let
        content =
            case model.page of
                HomePage homeModel ->
                    Page.Home.view homeModel |> Html.map GotHomeMsg

                LobbyPage lobbyModel ->
                    Page.Lobby.view lobbyModel |> Html.map GotLobbyMsg

                NotFoundPage _ ->
                    div [] [ text "Page not found" ]
    in
    { title = "dravid-19"
    , body = [ content ]
    }
