module ClientMsg exposing (ClientMsg(..), encodeMsg)

import GameState exposing (GameSettings)
import Json.Encode as E


type ClientMsg
    = Draw String
    | PickWord Int
    | Chat String
    | StartGame
    | Settings GameSettings


encodeMsg : ClientMsg -> E.Value
encodeMsg msg =
    let
        envelop : String -> E.Value -> E.Value
        envelop msgType m =
            E.object [ ( "type", E.string msgType ), ( "content", m ) ]
    in
    case msg of
        Draw data ->
            E.string data |> envelop "draw"

        PickWord index ->
            E.int index |> envelop "pick_word"

        Chat content ->
            E.string content |> envelop "chat"

        StartGame ->
            E.null |> envelop "start_game"

        Settings settings ->
            E.object
                [ ( "rounds", E.int settings.rounds )
                , ( "draw_time", E.int settings.drawSeconds )
                ]
                |> envelop "settings"
