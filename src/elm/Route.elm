module Route exposing (Route(..), parseRoute, routeParser)

import Url
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, s, string, top)
import Url.Parser.Query as Query


type Route
    = Home (Maybe String)
    | Lobby String
    | NotFound


parseRoute : Url.Url -> Route
parseRoute url =
    Url.Parser.parse routeParser url
        |> Maybe.withDefault NotFound


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map Home (top <?> Query.string "roomId")
        , map Lobby (s "rooms" </> string)
        ]
