module Drawable exposing (Drawable(..), fromString, toRenderable, toString)

import Canvas exposing (Point, Renderable, Shape, path, quadraticCurveTo, shapes)
import Canvas.Settings exposing (stroke)
import Canvas.Settings.Line exposing (LineCap(..), LineJoin(..), lineCap, lineJoin, lineWidth)
import Color exposing (Color)


type alias Point =
    ( Float, Float )


type Drawable
    = Spline Point Point Point Color Float
    | Clear


scalePoint : Point -> Point -> Point
scalePoint (w, h) (x, y) =
    (x * w, y * h)

toRenderable : (Float, Float) -> Drawable -> Renderable
toRenderable (w, h) drawable =
    let
        scale = scalePoint (w, h)
    in
    case drawable of
        Spline control start end color size ->
            let
                linePath =
                    path (scale start) [ quadraticCurveTo (scale control) (scale end) ]

                drawSize =
                    max 1 (round (w * size))
            in
            drawLine color drawSize linePath

        Clear ->
            Canvas.clear ( 0, 0 ) 4000 4000


drawLine : Color -> Int -> Shape -> Renderable
drawLine color size line =
    [ line ]
        |> shapes
            [ lineCap RoundCap
            , lineJoin RoundJoin
            , lineWidth (toFloat size)
            , stroke color
            ]


toString : Drawable -> String
toString drawable =
    case drawable of
        Spline control start end color size ->
            [ "s"
            , pointToString control
            , pointToString start
            , pointToString end
            , colorToString color
            , String.fromFloat size
            ]
                |> String.join ":"

        Clear ->
            "c"


fromString : String -> Maybe Drawable
fromString string =
    let
        parts =
            String.split ":" string
    in
    case parts of
        "s" :: [ control, start, end, color, size ] ->
            Maybe.map5 Spline
                (stringToPoint control)
                (stringToPoint start)
                (stringToPoint end)
                (stringToColor color)
                (String.toFloat size)

        "c" :: [] ->
            Just Clear

        _ ->
            Nothing


colorToString : Color -> String
colorToString color =
    let
        { red, green, blue, alpha } =
            Color.toRgba color
    in
    [ red, green, blue, alpha ]
        |> List.map String.fromFloat
        |> String.join ","


stringToColor : String -> Maybe Color
stringToColor string =
    let
        parts =
            String.split "," string |> List.map String.toFloat
    in
    case parts of
        [ Just r, Just g, Just b, Just a ] ->
            Just (Color.rgba r g b a)

        _ ->
            Nothing


pointToString : Point -> String
pointToString ( x, y ) =
    String.fromFloat x ++ "," ++ String.fromFloat y


stringToPoint : String -> Maybe Point
stringToPoint string =
    let
        parts =
            String.split "," string |> List.map String.toFloat
    in
    case parts of
        [ Just x, Just y ] ->
            Just ( x, y )

        _ ->
            Nothing
