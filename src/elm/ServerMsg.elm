module ServerMsg exposing
    ( ChatMsg
    , ServerMsg(..)
    , decode
    , decoder
    )

import GameState exposing (GameState, gameStateDecoder)
import Json.Decode as D


type ServerMsg
    = Sync GameState
    | Chat ChatMsg
    | Draw String


type alias ChatMsg =
    { playerId : String
    , message : String
    , system : Bool
    , color : Maybe String
    }


decode : D.Value -> Result D.Error ServerMsg
decode val =
    D.decodeValue decoder val


decoder : D.Decoder ServerMsg
decoder =
    let
        msgFromType : String -> D.Decoder ServerMsg
        msgFromType string =
            let
                msgDecoder =
                    case string of
                        "sync" ->
                            D.map Sync gameStateDecoder

                        "chat" ->
                            D.map Chat chatDecoder

                        "draw" ->
                            D.map Draw D.string

                        _ ->
                            D.fail ("Invalid msg type: " ++ string)
            in
            D.field "content" msgDecoder
    in
    D.field "type" D.string
        |> D.andThen msgFromType


chatDecoder : D.Decoder ChatMsg
chatDecoder =
    D.map4
        ChatMsg
        (D.field "player_id" D.string)
        (D.field "message" D.string)
        (D.field "system" D.bool)
        (D.maybe (D.field "color" D.string))
