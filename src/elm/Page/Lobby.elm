port module Page.Lobby exposing (Model, Msg(..), init, subscriptions, update, view)

import Array exposing (Array)
import Browser.Dom as Dom
import Browser.Events exposing (onAnimationFrameDelta, onResize)
import Browser.Navigation as Nav
import Canvas exposing (Point, Renderable)
import ClientMsg
import Color exposing (Color)
import Dict
import Drawable exposing (Drawable)
import GameState exposing (GameState, Player)
import Html exposing (Html, button, div, span, text)
import Html.Attributes as Attrs exposing (style)
import Html.Events exposing (onBlur, onClick, onInput)
import Html.Events.Extra.Mouse as Mouse
import Html.Events.Extra.Touch as Touch
import Identity exposing (Identity)
import Json.Decode as D
import Json.Encode as E
import Process
import ServerMsg exposing (ServerMsg)
import Task
import Time
import Url


port sendMessage : D.Value -> Cmd msg


port wsConnect : D.Value -> Cmd msg


port messageReceiver : (D.Value -> msg) -> Sub msg


type alias Model =
    { pending : List Renderable
    , remotePending : List Renderable
    , toDraw : List Renderable
    , toTransmit : List Drawable
    , pointer : Pointer
    , color : Color
    , size : Float
    , windowSize : ( Int, Int )
    , canvasSize : ( Float, Float )
    , lobbyId : String
    , identity : Identity
    , key : Nav.Key
    , game : GameState
    , chatHistory : List ServerMsg.ChatMsg
    , chatInput : String
    , drawHistory : List Drawable
    , url : Url.Url
    , settingsRoundsInput : String
    , settingsDrawTimeInput : String
    }


type Pointer
    = Drawing Point Point
    | NotDrawing
    | OffCanvas


init : Identity -> Nav.Key -> Url.Url -> String -> ( Model, Cmd Msg )
init identity key url lobbyId =
    let
        model =
            { pending = []
            , remotePending = []
            , toDraw = []
            , toTransmit = []
            , pointer = NotDrawing
            , color = Color.black
            , size = 0.01
            , windowSize = ( 0, 0 )
            , canvasSize = ( 0, 0 )
            , lobbyId = lobbyId
            , identity = identity
            , key = key
            , url = url
            , game = GameState.newGame
            , chatHistory = []
            , drawHistory = []
            , chatInput = ""
            , settingsRoundsInput = ""
            , settingsDrawTimeInput = ""
            }

        connectInfo =
            E.object [ ( "identity", Identity.encode identity ), ( "lobbyId", E.string lobbyId ) ]
    in
    if identity.name == "" then
        ( model, Nav.pushUrl model.key ("/?roomId=" ++ lobbyId) )

    else
        ( model, Cmd.batch [ initialSizeCmd, wsConnect connectInfo ] )


initialSizeCmd : Cmd Msg
initialSizeCmd =
    let
        viewportToMsg : Dom.Viewport -> Msg
        viewportToMsg viewport =
            WindowResize (round viewport.viewport.width) (round viewport.viewport.height)
    in
    Task.perform viewportToMsg Dom.getViewport


type Msg
    = AnimationFrame Float
    | WindowResize Int Int
    | StartAt ( Float, Float )
    | MoveAt ( Float, Float )
    | EndAt ( Float, Float )
    | EndOff ( Float, Float )
    | LeaveAt ( Float, Float )
    | ClearCanvas
    | SelectColor Color
    | SelectSize Float
    | TransmitDrawing
    | GotServerMsg (Result D.Error ServerMsg)
    | ChangeChatInput String
    | SendChat
    | NoOp
    | StartGame
    | PickWord Int
    | Repaint
    | ChangeRounds String
    | ChangeDrawSeconds String
    | SendSettings


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ pointer } as model) =
    let
        noCmd m =
            ( m, Cmd.none )

        delay time m =
            Process.sleep time
                |> Task.andThen (always <| Task.succeed m)
                |> Task.perform identity

        scalePoint : ( Float, Float ) -> ( Float, Float )
        scalePoint ( x, y ) =
            ( x / Tuple.first model.canvasSize, y / Tuple.second model.canvasSize )
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        AnimationFrame delta ->
            model
                |> flushPendingToDraw
                |> flushRemotePendingToDraw
                |> updateGameTimer delta
                |> noCmd

        WindowResize x y ->
            ( model |> resizeWindow x y, delay 100 Repaint )

        Repaint ->
            model |> repaint |> noCmd

        StartAt point ->
            initialPoint (scalePoint point) model |> noCmd

        MoveAt point ->
            case pointer of
                Drawing lastPoint lastControl ->
                    drawPoint (scalePoint point) lastPoint lastControl model |> noCmd

                OffCanvas ->
                    initialPoint (point |> scalePoint |> clampPoint) model |> noCmd

                NotDrawing ->
                    model |> noCmd

        EndAt point ->
            case pointer of
                Drawing lastPoint lastControl ->
                    model
                        |> drawPoint (scalePoint point) lastPoint lastControl
                        |> stopDrawing
                        |> noCmd

                OffCanvas ->
                    model |> stopDrawing |> noCmd

                NotDrawing ->
                    model |> noCmd

        EndOff _ ->
            model |> stopDrawing |> noCmd

        LeaveAt point ->
            case pointer of
                Drawing lastPoint lastControl ->
                    model
                        |> leaveCanvas (scalePoint point) lastPoint lastControl
                        |> noCmd

                _ ->
                    model |> noCmd

        ClearCanvas ->
            model |> clearCanvas |> noCmd

        SelectColor color ->
            selectColor color model |> noCmd

        SelectSize size ->
            selectSize size model |> noCmd

        TransmitDrawing ->
            if model.toTransmit == [] then
                model |> noCmd

            else
                let
                    data =
                        List.map Drawable.toString model.toTransmit
                            |> String.join "|"

                    drawMsg =
                        ClientMsg.encodeMsg (ClientMsg.Draw data)
                in
                ( { model | toTransmit = [] }, sendMessage drawMsg )

        GotServerMsg result ->
            case result of
                Ok serverMsg ->
                    handleServerMsg serverMsg model |> noCmd

                Err _ ->
                    model |> noCmd

        -- TODO : Show an error message if this happens
        ChangeChatInput message ->
            { model | chatInput = message } |> noCmd

        SendChat ->
            let
                encodedChatMsg =
                    ClientMsg.encodeMsg (ClientMsg.Chat model.chatInput)

                chatMsg =
                    ServerMsg.ChatMsg model.identity.hashId model.chatInput False Nothing
            in
            ( { model
                | chatInput = ""
                , chatHistory = chatMsg :: model.chatHistory
              }
            , Cmd.batch [ sendMessage encodedChatMsg, chatScroll ]
            )

        StartGame ->
            let
                startMsg =
                    ClientMsg.encodeMsg ClientMsg.StartGame
            in
            ( model, sendMessage startMsg )

        PickWord index ->
            let
                pickMsg =
                    ClientMsg.encodeMsg (ClientMsg.PickWord index)
            in
            ( model, sendMessage pickMsg )

        ChangeRounds rounds ->
            case model.game.gameMode of
                GameState.PreGame ->
                    ( { model | settingsRoundsInput = rounds }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ChangeDrawSeconds drawSeconds ->
            case model.game.gameMode of
                GameState.PreGame ->
                    ( { model | settingsDrawTimeInput = drawSeconds }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        SendSettings ->
            case model.game.gameMode of
                GameState.PreGame ->
                    let
                        settings =
                            GameState.GameSettings
                                (String.toInt model.settingsRoundsInput |> Maybe.withDefault 3)
                                (String.toInt model.settingsDrawTimeInput |> Maybe.withDefault 80)

                        settingsMsg =
                            ClientMsg.Settings settings
                    in
                    ( { model
                        | settingsRoundsInput = String.fromInt settings.rounds
                        , settingsDrawTimeInput = String.fromInt settings.drawSeconds
                      }
                    , sendMessage (ClientMsg.encodeMsg settingsMsg)
                    )

                _ ->
                    ( model, Cmd.none )


chatScroll : Cmd Msg
chatScroll =
    Dom.getViewportOf "chat"
        |> Task.andThen (\info -> Dom.setViewportOf "chat" 0 info.scene.height)
        |> Task.attempt (\_ -> NoOp)


flushPendingToDraw : Model -> Model
flushPendingToDraw ({ pending } as model) =
    { model | pending = [], toDraw = pending }


flushRemotePendingToDraw : Model -> Model
flushRemotePendingToDraw ({ remotePending, toDraw } as model) =
    case remotePending of
        head :: rest ->
            { model
                | remotePending = rest
                , toDraw = head :: toDraw
            }

        _ ->
            model


updateGameTimer : Float -> Model -> Model
updateGameTimer delta model =
    case model.game.gameMode of
        GameState.PreGame ->
            model

        _ ->
            let
                newDeadline =
                    max 0 (model.game.timeRemainingMs - round delta)

                gameState =
                    model.game

                updatedGameState =
                    { gameState | timeRemainingMs = newDeadline }
            in
            { model | game = updatedGameState }


selectColor : Color -> Model -> Model
selectColor color model =
    { model | color = color }


selectSize : Float -> Model -> Model
selectSize size model =
    { model | size = size }


repaint : Model -> Model
repaint model =
    { model | toDraw = List.map (Drawable.toRenderable model.canvasSize) model.drawHistory }


resizeWindow : Int -> Int -> Model -> Model
resizeWindow width height model =
    { model
        | windowSize = ( width, height )
        , canvasSize = canvasSize ( width, height )
    }


initialPoint : Point -> Model -> Model
initialPoint ( x, y ) model =
    { model | pointer = Drawing ( x, y ) ( x, y ) }


drawPoint : Point -> Point -> Point -> Model -> Model
drawPoint point lastPoint lastControl ({ pending, toTransmit } as model) =
    let
        controlPoint =
            calcControlPoint lastPoint point

        spline =
            Drawable.Spline lastPoint lastControl controlPoint model.color model.size
    in
    { model
        | pointer = Drawing point controlPoint
        , pending = Drawable.toRenderable model.canvasSize spline :: pending
        , drawHistory = spline :: model.drawHistory
        , toTransmit = spline :: toTransmit
    }


leaveCanvas : Point -> Point -> Point -> Model -> Model
leaveCanvas point lastPoint lastControl model =
    let
        edgePoint =
            clampPoint point

        spline =
            Drawable.Spline lastPoint lastControl edgePoint model.color model.size
    in
    { model
        | pointer = OffCanvas
        , pending = Drawable.toRenderable model.canvasSize spline :: model.pending
        , drawHistory = spline :: model.drawHistory
        , toTransmit = spline :: model.toTransmit
    }


clampPoint : Point -> Point
clampPoint ( x, y ) =
    let
        xEdgeDist =
            min (1 - x) x

        yEdgeDist =
            min (1 - y) y
    in
    if yEdgeDist < xEdgeDist then
        if y < 0.5 then
            ( x, 0 )

        else
            ( x, 1 )

    else if x < 0.5 then
        ( 0, y )

    else
        ( 1, y )


stopDrawing : Model -> Model
stopDrawing model =
    { model | pointer = NotDrawing }


calcControlPoint : Point -> Point -> Point
calcControlPoint ( x1, y1 ) ( x2, y2 ) =
    ( x1 + (x2 - x1) / 2, y1 + (y2 - y1) / 2 )


clearCanvas : Model -> Model
clearCanvas model =
    let
        clear =
            Drawable.Clear
    in
    { model
        | pending = Drawable.toRenderable model.canvasSize clear :: model.pending
        , drawHistory = []
        , toTransmit = clear :: model.toTransmit
    }


handleServerMsg : ServerMsg -> Model -> Model
handleServerMsg serverMsg model =
    case serverMsg of
        ServerMsg.Draw data ->
            let
                drawables =
                    String.split "|" data
                        |> List.map Drawable.fromString
                        |> List.filterMap identity

                renderables =
                    drawables
                        |> List.map (Drawable.toRenderable model.canvasSize)
                        |> List.reverse
            in
            { model
                | remotePending = model.remotePending ++ renderables
                , drawHistory = model.drawHistory ++ drawables
            }

        ServerMsg.Sync game ->
            let
                clear : Model -> Model
                clear m =
                    case game.gameMode of
                        GameState.WordSelect _ ->
                            { m
                                | pending = [ Drawable.toRenderable model.canvasSize Drawable.Clear ]
                                , remotePending = []
                                , drawHistory = []
                            }

                        _ ->
                            m

                updateGame : GameState -> Model -> Model
                updateGame g m =
                    { m
                        | game = g
                        , settingsRoundsInput = String.fromInt game.settings.rounds
                        , settingsDrawTimeInput = String.fromInt game.settings.drawSeconds
                    }
            in
            model
                |> clear
                |> updateGame game

        ServerMsg.Chat chatMsg ->
            { model | chatHistory = chatMsg :: model.chatHistory }


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        standardSubs =
            Sub.batch
                [ onAnimationFrameDelta AnimationFrame
                , onResize WindowResize
                , messageReceiver (ServerMsg.decode >> GotServerMsg)
                ]
    in
    case model.game.gameMode of
        GameState.Drawing ->
            if model.game.drawer == model.identity.hashId then
                Sub.batch
                    [ messageReceiver (ServerMsg.decode >> GotServerMsg)
                    , onResize WindowResize
                    , onAnimationFrameDelta AnimationFrame
                    , Time.every 100 (\_ -> TransmitDrawing)
                    ]

            else
                standardSubs

        GameState.WordSelect _ ->
            standardSubs

        GameState.TurnOver ->
            standardSubs

        GameState.GameOver ->
            standardSubs

        GameState.PreGame ->
            Sub.batch
                [ onResize WindowResize
                , messageReceiver (ServerMsg.decode >> GotServerMsg)
                ]


view : Model -> Html Msg
view model =
    case model.game.gameMode of
        GameState.PreGame ->
            viewPreGame model

        _ ->
            viewPlaying model


viewPreGame : Model -> Html Msg
viewPreGame model =
    div [ Attrs.class "pre-game-wrapper" ]
        [ div [ Attrs.class "pre-game-main" ]
            [ viewLobby model
            , viewChat model "Enter a message"
            ]
        ]


viewStartButton : Model -> Bool -> Html Msg
viewStartButton model isHost =
    if isHost then
        let
            canStart =
                Dict.size model.game.players > 1
        in
        div [ Attrs.class "start-button-wrapper" ]
            [ Html.button
                [ onClick StartGame
                , Attrs.disabled (not canStart)
                ]
                [ text "Start Game" ]
            ]

    else
        text ""


viewLobby : Model -> Html Msg
viewLobby model =
    let
        isHost =
            model.game.host == model.identity.hashId
    in
    div [ Attrs.class "game-lobby" ]
        [ viewSettings model isHost
        , viewPlayersPreGame model
        , viewStartButton model isHost
        ]


viewSettings : Model -> Bool -> Html Msg
viewSettings model isHost =
    Html.form []
        [ Html.h2 [] [ text "Settings" ]
        , div []
            [ Html.label [] [ text "Rounds" ]
            , Html.input
                [ Attrs.value model.settingsRoundsInput
                , onInput ChangeRounds
                , onBlur SendSettings
                , Attrs.disabled (not isHost)
                ]
                []
            ]
        , div []
            [ Html.label [] [ text "Draw Time" ]
            , Html.input
                [ Attrs.value model.settingsDrawTimeInput
                , Attrs.disabled (not isHost)
                , onInput ChangeDrawSeconds
                , onBlur SendSettings
                ]
                []
            , Html.span [] [ text " seconds" ]
            ]
        ]


viewPlayersPreGame : Model -> Html Msg
viewPlayersPreGame model =
    let
        flip fn b a =
            fn a b

        url =
            String.split "/" (Url.toString model.url)
                |> List.take 3
                |> flip List.append [ "?roomId=" ++ model.lobbyId ]
                |> String.join "/"
    in
    div []
        [ div [ Attrs.class "player-list-title" ]
            [ Html.h2 [] [ text "Players" ]
            , viewShareLink url
            ]
        , div [ Attrs.class "pre-game-players" ] <|
            List.map (viewPlayerCard model) (Dict.values model.game.players)
        ]


viewPlaying : Model -> Html Msg
viewPlaying model =
    let
        controls =
            if model.game.drawer == model.identity.hashId then
                viewControls model

            else
                text ""

        wordSelectionModal =
            case model.game.gameMode of
                GameState.WordSelect words ->
                    if model.identity.hashId == model.game.drawer then
                        viewModal <| viewWordSelection words

                    else
                        viewModal <| viewOtherWordSelection model

                _ ->
                    text ""

        standingsModal =
            case model.game.gameMode of
                GameState.TurnOver ->
                    viewModal <| viewStandings model False

                GameState.GameOver ->
                    viewModal <| viewStandings model True

                _ ->
                    text ""
    in
    div [ Attrs.class "game-main", Mouse.onUp (.offsetPos >> EndOff) ]
        [ wordSelectionModal
        , viewPlayerList model
        , div [ Attrs.class "game-right" ]
            [ div [ Attrs.class "game-canvas-chat" ]
                [ viewGame model
                , viewChat model "Make a guess"
                ]
            , controls
            ]
        , standingsModal
        ]


viewModal : Html Msg -> Html Msg
viewModal content =
    div [ Attrs.class "modal-container" ]
        [ div [ Attrs.class "modal" ] [ content ]
        ]


viewStandings : Model -> Bool -> Html Msg
viewStandings model gameOver =
    let
        title =
            if gameOver then
                "Game Over"

            else
                "Standings"

        viewPlayer place ( pid, score ) =
            let
                name =
                    GameState.getPlayerName model.game pid

                self =
                    model.identity.hashId == pid

                points =
                    if gameOver then
                        Nothing

                    else
                        Dict.get pid model.game.winners
            in
            viewPlayerStanding name (place + 1) score points self

        cmp ( _, s1 ) ( _, s2 ) =
            compare s2 s1

        standings =
            Dict.toList model.game.scores
                |> List.sortWith cmp
                |> List.indexedMap viewPlayer
    in
    div [ Attrs.class "standings" ]
        (Html.h2 [] [ text title ] :: standings)


viewPlayerStanding : String -> Int -> Int -> Maybe Int -> Bool -> Html Msg
viewPlayerStanding name place score points self =
    let
        ordinal p =
            case p of
                1 ->
                    "1st"

                2 ->
                    "2nd"

                3 ->
                    "3rd"

                _ ->
                    ""

        viewPoints p =
            String.fromInt p

        viewNewPoints p =
            "(+" ++ String.fromInt p ++ ")"

        viewScoredPoints p =
            span [ Attrs.class "standing-points" ] [ text <| viewNewPoints p ]

        scoredPoints =
            Maybe.map viewScoredPoints points |> Maybe.withDefault (text "")
    in
    div [ Attrs.classList [ ( "standing-player", True ), ( "self", self ) ] ]
        [ span [ Attrs.class "standing-place" ] [ text (ordinal place) ]
        , span [ Attrs.class "standing-name" ] [ text name ]
        , span [ Attrs.class "standing-score" ] [ text <| viewPoints score ]
        , scoredPoints
        ]


viewWordSelection : List String -> Html Msg
viewWordSelection words =
    let
        viewWordOption index word =
            Html.button [ onClick (PickWord index) ] [ text word ]
    in
    div [ Attrs.class "word-options" ] (List.indexedMap viewWordOption words)


viewOtherWordSelection : Model -> Html Msg
viewOtherWordSelection model =
    let
        name =
            Dict.get model.game.drawer model.game.players
                |> Maybe.map (\p -> p.name)
                |> Maybe.withDefault "<unknown>"
    in
    div [ Attrs.class "other-word-selection" ] [ text (name ++ " is choosing a word...") ]


viewPlayerList : Model -> Html Msg
viewPlayerList model =
    let
        flip fn b a =
            fn a b

        players =
            List.map (flip Dict.get model.game.players) model.game.turnOrder
                |> List.filterMap identity
    in
    div
        [ Attrs.class "game-players" ]
        (List.map (viewPlayerCard model) players)


viewPlayerCard : Model -> GameState.Player -> Html Msg
viewPlayerCard model player =
    let
        isHost =
            model.game.host == player.id && model.game.gameMode == GameState.PreGame

        isDrawing =
            model.game.drawer == player.id

        isWinner =
            Dict.member player.id model.game.winners

        score =
            case model.game.gameMode of
                GameState.PreGame ->
                    ""

                _ ->
                    Dict.get player.id model.game.scores
                        |> Maybe.withDefault 0
                        |> String.fromInt

        color =
            modBy (Array.length nameColors) player.color
                |> (\n -> Array.get n nameColors)
                |> Maybe.withDefault Color.lightBlue

        viewScore =
            if score == "" then
                text ""

            else
                Html.div [ Attrs.class "player-score" ] [ text (score ++ " points") ]

        nameIcons =
            [ ( "icon-pencil", isDrawing ), ( "icon-crown", isHost ) ]
                |> List.filterMap
                    (\( n, show ) ->
                        if show then
                            Just n

                        else
                            Nothing
                    )

        viewIcon name =
            Html.i [ Attrs.class name ] []
    in
    div
        [ Attrs.classList
            [ ( "player-card", True )
            , ( "player-winner", isWinner )
            , ( "player-disconnected", not player.connected )
            ]
        , Attrs.style "background-color" (Color.toCssString color)
        ]
    <|
        [ Html.img [ Attrs.src "/img/avatar.png" ] []
        , Html.div [ Attrs.class "player-name" ] [ text player.name ]
        , viewScore
        ]
            ++ List.map viewIcon nameIcons


viewGame : Model -> Html Msg
viewGame ({ toDraw, windowSize } as model) =
    let
        ( width, height ) =
            canvasSize windowSize

        canvasAttrs =
            if model.game.drawer == model.identity.hashId then
                [ Mouse.onDown (.offsetPos >> StartAt)
                , Mouse.onMove (.offsetPos >> MoveAt)
                , Mouse.onUp (.offsetPos >> EndAt)
                , style "touch-action" "none"
                , Mouse.onLeave (.offsetPos >> LeaveAt)
                , onTouch "touchstart" (touchCoordinates >> StartAt)
                , onTouch "touchmove" (touchCoordinates >> MoveAt)
                , onTouch "touchend" (touchCoordinates >> EndAt)
                ]

            else
                []
    in
    div [ Attrs.class "game" ]
        [ viewGameState model
        , Canvas.toHtml ( round width, round height ) canvasAttrs toDraw
        ]


viewGameState : Model -> Html Msg
viewGameState model =
    let
        seconds =
            toFloat model.game.timeRemainingMs
                / 1000
                |> round
                |> String.fromInt
    in
    div [ Attrs.class "game-state" ]
        [ div
            [ Attrs.class "game-round" ]
            [ text ("Round " ++ String.fromInt model.game.round) ]
        , div
            [ Attrs.class "game-timer" ]
            [ span [ Attrs.class "timer-amount" ] [ text seconds ]
            , span [ Attrs.class "timer-units" ] [ text " seconds left" ]
            ]
        , div
            [ Attrs.class "game-word" ]
            [ text model.game.word ]
        ]


canvasSize : ( Int, Int ) -> ( Float, Float )
canvasSize ( windowWidth, windowHeight ) =
    let
        ratio =
            2 / 3

        calcHeight w =
            w * ratio

        width =
            2000
                |> min (toFloat windowWidth - 600)
                |> min (toFloat (windowHeight - 200) / ratio)
    in
    ( width, calcHeight width )


viewPlayerListOld : Model -> Html Msg
viewPlayerListOld model =
    let
        viewPlayer : String -> Player -> Html Msg
        viewPlayer host player =
            let
                hostTag =
                    if host == player.id then
                        "*"

                    else
                        ""
            in
            div [] [ text (hostTag ++ player.name) ]
    in
    div [ Attrs.class "player-list" ] <|
        div [] [ text "Players" ]
            :: List.map (viewPlayer model.game.host) (Dict.values model.game.players)


viewChat : Model -> String -> Html Msg
viewChat model placeholder =
    let
        name userId =
            Dict.get userId model.game.players
                |> Maybe.map (\p -> p.name)
                |> Maybe.withDefault "<unknown>"

        color userId =
            Dict.get userId model.game.players
                |> Maybe.map (\p -> p.color)
                |> Maybe.withDefault 0
                |> modBy (Array.length nameColors)
                |> (\n -> Array.get n nameColors)
                |> Maybe.withDefault Color.black

        winner =
            Dict.member model.identity.hashId model.game.winners

        realPlaceholder =
            if winner then
                "You guessed the word!"

            else
                placeholder

        viewChatMsg : ServerMsg.ChatMsg -> Html Msg
        viewChatMsg msg =
            if msg.system then
                div [ Attrs.class "chat-row" ]
                    [ span
                        [ Attrs.class "chat-row-msg chat-row-system"
                        , Attrs.style "color" (msg.color |> Maybe.withDefault "black")
                        ]
                        [ text msg.message ]
                    ]

            else
                div [ Attrs.class "chat-row" ]
                    [ span
                        [ Attrs.class "chat-row-name"
                        , Attrs.style "color" (Color.toCssString (color msg.playerId))
                        ]
                        [ text (name msg.playerId) ]
                    , span
                        [ Attrs.class "chat-row-msg"
                        ]
                        [ text (": " ++ msg.message) ]
                    ]
    in
    div [ Attrs.class "right-container" ]
        [ div [ Attrs.class "chat-container" ]
            [ div [ Attrs.id "chat", Attrs.class "chat" ] (List.map viewChatMsg model.chatHistory)
            , Html.form [ Html.Events.onSubmit SendChat ]
                [ Html.input
                    [ onInput ChangeChatInput
                    , Attrs.placeholder realPlaceholder
                    , Attrs.value model.chatInput
                    , Attrs.classList [ ( "winner", winner ) ]
                    ]
                    []
                , Html.button
                    [ Attrs.type_ "submit", Attrs.disabled (model.chatInput == "") ]
                    [ text "Send" ]
                ]
            ]
        ]


viewShareLink : String -> Html Msg
viewShareLink link =
    div [ Attrs.class "room-link" ]
        [ Html.span [] [ text "Share " ]
        , Html.input [ Attrs.readonly True, Attrs.value link ] []
        ]


viewControls : Model -> Html Msg
viewControls model =
    div [ Attrs.class "controls" ]
        [ viewSizeControls (Tuple.first model.canvasSize) model.color model.size
        , viewColorControls model.color
        , viewOtherControls
        ]


viewSizeControls : Float -> Color -> Float -> Html Msg
viewSizeControls canvasWidth selectedColor selectedSize =
    let
        brushSizes =
            [ 0.005, 0.01, 0.03, 0.05 ]

        viewButton size =
            viewSizeButton canvasWidth selectedSize selectedColor 38 size

        controls =
            List.map viewButton brushSizes
    in
    div [ Attrs.class "size-controls" ] controls


viewSizeButton : Float -> Float -> Color -> Int -> Float -> Html Msg
viewSizeButton canvasWidth selectedSize selectedColor buttonSize size =
    let
        displaySize =
            round (size * canvasWidth)
    in
    button
        [ style "min-height" (String.fromInt buttonSize ++ "px")
        , onClick (SelectSize size)
        ]
        [ div
            [ style "background-color" (Color.toCssString selectedColor)
            , style "width" (String.fromInt displaySize ++ "px")
            , style "height" (String.fromInt displaySize ++ "px")
            , Attrs.classList [ ( "selected", selectedSize == size ) ]
            ]
            []
        ]


nameColors : Array Color
nameColors =
    Array.fromList
        [ Color.rgb255 249 65 68
        , Color.rgb255 243 114 44
        , Color.rgb255 248 150 30
        , Color.rgb255 249 132 74
        , Color.rgb255 249 199 79
        , Color.rgb255 144 190 109
        , Color.rgb255 67 170 139
        , Color.rgb255 77 144 142
        , Color.rgb255 87 117 144
        , Color.rgb255 39 125 161
        ]


viewColorControls : Color -> Html Msg
viewColorControls selectedColor =
    let
        col btns =
            div [] btns

        layout colors =
            colors
                |> List.map (List.map (viewColorButton selectedColor) >> col)
    in
    div [ Attrs.class "color-controls" ] <|
        layout
            [ [ Color.lightRed
              , Color.red

              -- , Color.darkRed
              ]
            , [ Color.lightOrange
              , Color.orange

              -- , Color.darkOrange
              ]
            , [ Color.lightYellow
              , Color.yellow

              -- , Color.darkYellow
              ]
            , [ Color.lightGreen
              , Color.green

              -- , Color.darkGreen
              ]
            , [ Color.lightBlue
              , Color.blue

              -- , Color.darkBlue
              ]
            , [ Color.lightPurple
              , Color.purple

              -- , Color.darkPurple
              ]
            , [ Color.lightBrown
              , Color.brown

              -- , Color.darkBrown
              ]
            , [ Color.white
              , Color.lightGrey

              -- , Color.grey
              ]
            , [ Color.darkGrey
              , Color.lightCharcoal

              -- , Color.charcoal
              ]
            , [ Color.darkCharcoal
              , Color.black

              -- , Color.lightGrey
              ]
            ]


viewColorButton : Color -> Color -> Html Msg
viewColorButton selectedColor color =
    button
        [ Attrs.classList [ ( "color-button", True ), ( "selected", selectedColor == color ) ]
        , style "background-color" (Color.toCssString color)
        , onClick (SelectColor color)
        ]
        []


viewOtherControls : Html Msg
viewOtherControls =
    div [ Attrs.class "other-controls" ]
        [ button [ onClick ClearCanvas ] [ Html.i [ Attrs.class "icon-trash" ] [] ]
        , button [ onClick (SelectColor Color.lightGrey) ] [ Html.i [ Attrs.class "icon-eraser" ] [] ]
        ]


touchCoordinates : { event : Touch.Event, targetOffset : ( Float, Float ) } -> ( Float, Float )
touchCoordinates { event, targetOffset } =
    List.head event.changedTouches
        |> Maybe.map
            (\touch ->
                let
                    ( x, y ) =
                        touch.pagePos

                    ( x2, y2 ) =
                        targetOffset
                in
                ( x - x2, y - y2 )
            )
        |> Maybe.withDefault ( 0, 0 )


onTouch event tag =
    eventDecoder
        |> D.map
            (\ev ->
                { message = tag ev
                , preventDefault = True
                , stopPropagation = True
                }
            )
        |> Html.Events.custom event


eventDecoder =
    D.map2
        (\event offset ->
            { event = event
            , targetOffset = offset
            }
        )
        Touch.eventDecoder
        offsetDecoder


offsetDecoder =
    D.field "target"
        (D.map2 (\top left -> ( left, top ))
            (D.field "offsetTop" D.float)
            (D.field "offsetLeft" D.float)
        )
