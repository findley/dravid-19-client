module Page.Home exposing (Model, Msg(..), init, subscriptions, update, view)

import Browser.Navigation as Nav
import Html exposing (Html, div, text)
import Html.Attributes as Attrs
import Html.Events as Events
import Identity exposing (Identity)
import Random


type alias Model =
    { lobbyId : Maybe String
    , key : Nav.Key
    , identity : Identity
    }


init : Identity -> Nav.Key -> Maybe String -> ( Model, Cmd Msg )
init identity key lobbyId =
    ( { lobbyId = lobbyId, key = key, identity = identity }, Cmd.none )


type Msg
    = ChangeName String
    | Start
    | GotNewLobbyId String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        noCmd m =
            ( m, Cmd.none )
    in
    case msg of
        ChangeName name ->
            let
                currentIdentity =
                    model.identity

                identity =
                    { currentIdentity | name = name }
            in
            { model | identity = identity } |> noCmd

        Start ->
            case model.lobbyId of
                Just id ->
                    ( model, Nav.pushUrl model.key ("/rooms/" ++ id) )

                Nothing ->
                    let
                        generator =
                            Random.int 1 999999999

                        messageMapper =
                            String.fromInt >> GotNewLobbyId
                    in
                    ( model, Random.generate messageMapper generator )

        GotNewLobbyId id ->
            ( model, Nav.pushUrl model.key ("/rooms/" ++ id) )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


view : Model -> Html Msg
view model =
    let
        buttonText =
            case model.lobbyId of
                Just _ ->
                    "Join Room"

                Nothing ->
                    "Create Room"
    in
    div [ Attrs.class "home-container" ]
        [ div [ Attrs.class "home-card" ]
            [ Html.h1 [] [ text "dravid-19" ]
            , Html.form [ Events.onSubmit Start, Attrs.class "home-form" ]
                [ Html.input
                    [ Attrs.name "name"
                    , Attrs.placeholder "Name"
                    , Attrs.value model.identity.name
                    , Attrs.autofocus True
                    , Attrs.spellcheck False
                    , Events.onInput ChangeName
                    ]
                    []
                , Html.button
                    [ Attrs.type_ "submit", Attrs.disabled (model.identity.name == "") ]
                    [ Html.text buttonText ]
                ]
            ]
        ]
