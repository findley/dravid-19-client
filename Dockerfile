FROM tortis/alpine-elm-ci:latest as build

USER root
RUN mkdir -p /app
WORKDIR /app

COPY package*.json ./
COPY elm.json ./

RUN npm ci

COPY . .

RUN npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
