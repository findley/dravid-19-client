# Todo

## Bugs
- [ ] Safari - can't chat after making correct guess
- [x] Stop brush from sticking after going off canvas
- [x] Fix default brush size (to smaller brush)
- [x] Previous drawing not getting cleared after turn ends
- [x] Turn order acting weird (sometimes player gets 2 consecutive draw turns?)
- [x] Fix word options (send with game sync instead of separate message)
- [x] Chat container growing indefinitely

## Core
- [ ] End of turn points summary sequence
- [ ] Player list rank indicator
- [x] Player list drawer indicator
- [x] Player list host indicator
- [x] Indicate if player is disconnected
- [x] System chat messages
- [x] Add share link to lobby pre-game view
- [x] Redraw history after resize
- [x] Canvas draw commands scaled 0->1
- [x] Ability to set game settings

## Polish
- [ ] Better layout (responsive mobile support/grid system)
- [ ] Catchup mechanic for flushing remote Drawables to canvas
- [ ] Go to home if webocket disconnects
- [ ] Generate color from userId
- [x] Handle reconnecting player
- [x] Better feedback on correct guess of word
- [x] Make words/guesses case insensitive
- [x] Prevent start game with 1 player (or just go to free draw mode?)

## Infrastructure
- [ ] Deploy with alpine/k8s to avoid curl of kubectl during deployment
- [ ] Binary encoding for draw messages

## Features
- [ ] Game sound effects
- [ ] Points based on difficulty of word
- [ ] Inactivity disconnect
- [ ] Cursor to distinguish pen and eraser (and fill bucket in future)
- [ ] Handle pressure sensitivity
- [ ] Build fill tool
- [ ] Prebuilt word lists (categories)
- [ ] Remember word history among private room group to prevent repeats
- [ ] Custom word list
- [ ] Personal drawn avatar (save in local storage?)
- [ ] Create bots by recording human players
- [ ] Add undo support for drawing
